//Angela Sposato 1934695
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class Vector3dTests {

	@Test
	public void testVector3d() {
		Vector3d testVector = new Vector3d(1, 1, 2);
		assertEquals(1, testVector.getX());
		assertEquals(1, testVector.getY());
		assertEquals(2, testVector.getZ());
	}
	
	@Test
	public void testMagnitude() {
		Vector3d testVector = new Vector3d(4, 0, 0);
		assertEquals(4, testVector.magnitude());
	}
	
	@Test
	public void testDotProduct() {
		Vector3d testVector = new Vector3d(1, 1, 2);
		Vector3d testVector2 = new Vector3d(2, 3, 4);
		assertEquals(13, testVector.dotProduct(testVector2));
	}
	
	@Test
	public void testAdd() {
		Vector3d testVector = new Vector3d(1, 1, 2);
		Vector3d testVector2 = new Vector3d(2, 3, 4);
		assertEquals(3, testVector.add(testVector2).getX());
		assertEquals(4, testVector.add(testVector2).getY());
		assertEquals(6, testVector.add(testVector2).getZ());
	}
}


