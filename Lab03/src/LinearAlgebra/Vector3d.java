//Angela Sposato 1934695	
package LinearAlgebra;

public class Vector3d {
	double x;
	double y; 
	double z;
	public Vector3d(double x, double y, double z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getZ() {
		return z;
	}
	
	public double magnitude() {
		return Math.sqrt((this.x*this.x)+(this.y*this.y)+(this.z*this.z));
	}
	
	public double dotProduct(Vector3d vector) {
		return ((this.x*vector.x)+(this.y*vector.y)+(this.z*vector.z));
	}
	
	public Vector3d add(Vector3d vector) {
		Vector3d newVector = new Vector3d ((this.x + vector.x), (this.y+vector.y), (this.z+vector.z));
		return newVector;
	}
	
	public String toString() {
		return x + ", " + y + ", " + z; 
	}
}

	
	
